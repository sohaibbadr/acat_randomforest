import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn import tree
from sklearn.ensemble import RandomForestRegressor
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)
import matplotlib.pyplot as plt
from sklearn.model_selection import RandomizedSearchCV
#--------------------------
df = pd.read_csv('params_HR.csv',sep='\t',header = 0)
df.rename( columns={'Unnamed: 0':'a'}, inplace=True )
df_features = df.drop(['a','C_max', 't_max', 'AUC', 'AUC_inf', 'AUC/dose', 'MRT'],axis =1)
X1 = df[[ 'Kpuu_colon', 'Vmax_met_vitro', 'BDM', 'Km_met_vitro', 'Kpuu_liver', 'dose']].values
X2 = df[[ 'Kpuu_colon', 'BDM', 'Kpuu_liver', 'Vmax_met_vitro', 'Km_met_vitro']].values
X3 = df[[ 'Kpuu_colon', 'BDM', 'Km_met_vitro', 'Vmax_met_vitro', 'Kpuu_liver']].values
Y = df['C_max'].values
Z = df['t_max'].values
M = df['AUC/dose'].values
#----------------------
X1_train, X1_test, y_train, y_test = train_test_split(X1, Y,test_size = 0.35,random_state = 42)
X2_train, X2_test, z_train, z_test = train_test_split(X2, Z,test_size = 0.4,random_state = 42)
X3_train, X3_test, m_train, m_test = train_test_split(X3, M,test_size = 0.4,random_state = 42)

#------------------
# hyper parameters varition defenition
#hyperparameters optimazation
max_features =['auto','sqrt','log2']
min_samples_split = [2,4,8]
min_samples_leaf = [2,4,8]
random_grid ={'max_features': max_features,
           'min_samples_split': min_samples_split,
           'min_samples_leaf': min_samples_leaf,
           'n_estimators' :[150]
             }
# Random Forest
#----------------------
clf_cmax = RandomForestRegressor()
clf_random_cmax = RandomizedSearchCV(estimator = clf_cmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
clf_random_cmax.fit(X1_train,y_train)
optimized_clf_cmax = clf_random_cmax.best_params_
clf_cmax.set_params(max_features=optimized_clf_cmax['max_features'],min_samples_split=optimized_clf_cmax['min_samples_split'],
           min_samples_leaf=optimized_clf_cmax['min_samples_leaf'],n_estimators=optimized_clf_cmax['n_estimators']
              )
clf_cmax.fit(X1_train,y_train)
#-------------------------
clf_tmax = RandomForestRegressor()
clf_random_tmax = RandomizedSearchCV(estimator = clf_tmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
clf_random_tmax.fit(X2_train,z_train)
optimized_clf_tmax = clf_random_tmax.best_params_
clf_tmax.set_params(max_features=optimized_clf_tmax['max_features'],min_samples_split=optimized_clf_tmax['min_samples_split'],
           min_samples_leaf=optimized_clf_tmax['min_samples_leaf'],n_estimators=optimized_clf_tmax['n_estimators']
              )
clf_tmax.fit(X2_train,z_train)

#------------------------
clf_AUC = RandomForestRegressor()
clf_random_AUC = RandomizedSearchCV(estimator = clf_AUC, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
clf_random_AUC.fit(X3_train,m_train)
optimized_clf_AUC = clf_random_AUC.best_params_
clf_AUC.set_params(max_features=optimized_clf_AUC['max_features'],min_samples_split=optimized_clf_AUC['min_samples_split'],
           min_samples_leaf=optimized_clf_AUC['min_samples_leaf'],n_estimators=optimized_clf_AUC['n_estimators']
              )
clf_AUC.fit(X3_train,m_train)
#----------------------

cmax_predict = clf_cmax.predict(X1_test)
tmax_predict = clf_tmax.predict(X2_test)
AUC_predict = clf_AUC.predict(X3_test)

cmax_predict_train =clf_cmax.predict(X1_train)
tmax_predict_train = clf_tmax.predict(X2_train)
AUC_predict_train = clf_AUC.predict(X3_train)

#-------------------------------
Accurecy_test = np.array([mean_squared_error(y_test, cmax_predict),mean_squared_error(z_test, tmax_predict),mean_squared_error(m_test, AUC_predict)])
Accurecy_train = np.array([mean_squared_error(y_train, cmax_predict_train),mean_squared_error(z_train, tmax_predict_train),mean_squared_error(m_train, AUC_predict_train)])
#------------------------------
# Note : if the values of the training and testing MSE are close enough then the fitting is perfect If not then the model has been either overfitted or underfitted
# The best value for MSE is zero
print('MSE of testing data')
print(Accurecy_test)
print('MSE of training data')
print(Accurecy_train)

cmax_error = np.zeros(len(y_test))
tmax_error = np.zeros(len(z_test))
AUC_error = np.zeros(len(m_test))

for i in range(len(y_test)):
    cmax_error[i] = ((y_test[i]-cmax_predict[i])/y_test[i])*100
    tmax_error[i] = ((tmax_predict[i]-z_test[i])/z_test[i])*100
    AUC_error[i] = ((m_test[i]-AUC_predict[i])/m_test[i])*100
#------------------------
cmax_error = [a for a in cmax_error]
tmax_error = [a for a in tmax_error]
AUC_error = [a for a in AUC_error]

#-----------------------
BINS_cmax = np.linspace(-100,100,11)
BINS_cmax = [x for x in BINS_cmax]
BINS_tmax = np.linspace(-100,100,11)
BINS_tmax = [x for x in BINS_tmax]
BINS_AUC = np.linspace(-100,100,11)
BINS_AUC = [x for x in BINS_tmax]
#-----------------------
y_hb = y_test*1.1
y_lb = y_test*0.9
y_hb = [a for a in y_hb]
y_lb = [a for a in y_lb]
#--------------
z_hb = z_test*1.1
z_lb = z_test*0.9
z_hb = [a for a in z_hb]
z_lb = [a for a in z_lb]
#----------------
m_hb = m_test*1.1
m_lb = m_test*0.9
m_hb = [a for a in m_hb]
m_lb = [a for a in m_lb]
#-----------------
y_test =[a for a in y_test]
cmax_predict = [a for a in cmax_predict]
z_test =[a for a in z_test]
tmax_predict = [a for a in tmax_predict]
m_test =[a for a in m_test]
AUC_predict = [a for a in AUC_predict]
#------------------------

plt.figure(1)
plt.subplot(131)
plt.hist(cmax_error,bins =BINS_cmax,color = 'red',ec ='black', normed = 1)
plt.xlabel('%Error')
plt.ylabel('Probability/binwidth',fontsize =8)
plt.xticks(np.asanyarray(BINS_cmax))
plt.xticks(rotation = 90)
plt.title('C_max Classifier accurecy_Reduced ',fontsize = 10)
plt.grid()
#--------------------
plt.subplot(132)
plt.hist(tmax_error,bins =BINS_tmax, color = 'red',ec = 'black',normed = 1)
plt.xlabel('%Error')
plt.title('t_max Classifier accurecy__Reduced ',fontsize =8)
plt.xticks(np.asanyarray(BINS_tmax))
plt.xticks(rotation = 90)
plt.grid()
#-------------------
plt.subplot(133)
plt.hist(AUC_error,bins =BINS_AUC, color = 'red',ec = 'black',normed =1)
plt.xlabel('%Error')
plt.title('(Clearance^(-1)) Classifier accurecy__Reduced ',fontsize =8)
plt.xticks(np.asanyarray(BINS_AUC))
plt.xticks(rotation = 90)
plt.grid()


#---------------------
plt.figure(2)
plt.subplot(131)
plt.plot(y_test,y_test, label = 'perfect prediction')
plt.plot(y_test,y_hb, label = '+ 10 % prediction')
plt.plot(y_test,y_lb, label = '- 10 % prediction')
plt.plot(y_test,cmax_predict,'b.',label = 'Actual prediction')
plt.legend()
plt.xlabel('True(Model)')
plt.ylabel('predicted(Classifier)')
plt.title('C_max classifier')
plt.grid(which = 'major')
plt.subplot(132)
plt.plot(z_test,z_test,label = 'perfect prediction')
plt.plot(z_test,z_hb, label = '+ 10 % prediction')
plt.plot(z_test,z_lb, label = '- 10 % prediction')
plt.plot(z_test,tmax_predict,'b.',label = 'Actual prediction')
plt.xlabel('True(Model)')
plt.ylabel('predicted(Classifier)')
plt.legend()
plt.title('t_max classifier')
plt.grid(which = 'major')
plt.subplot(133)
plt.plot(m_test,m_test,label = 'perfect prediction' )
plt.plot(m_test,m_hb, label = '+ 10 % prediction')
plt.plot(m_test,m_lb, label = '- 10 % prediction')
plt.plot(m_test,AUC_predict,'b.', label = 'Actual prediction' )
plt.xlabel('True(Model)')
plt.ylabel('predicted(Classifier)')
plt.legend()
plt.title('AUC/dose(clearance^(-1)) classifier')
plt.grid(which = 'major')
plt.legend()
plt.show()
#-------------------
pickle.dump(clf_cmax,open('clf_cmax.sav','wb'))
pickle.dump(clf_tmax,open('clf_tmax.sav','wb'))
pickle.dump(clf_AUC,open('clf_AUC.sav','wb'))
