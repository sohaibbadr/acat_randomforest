import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.datasets import make_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)
#-----------------------
n_per = 2
n_features = 19
df = pd.read_csv('params_HR.csv',sep='\t',header = 0)
df.rename( columns={'Unnamed: 0':'a'}, inplace=True )

df_features = df.drop(['a','C_max', 't_max', 'AUC', 'AUC_inf', 'AUC/dose', 'MRT'],axis =1)

X =  (df.loc[:,['G_Density', 'MM', 'Peff', 'dose', 'Mol_vol', 'pKa', 'G_Radius', 'Kpuu_cecum',
                  'K_precip', 'Kpuu_ileum', 'Kpuu_jeju', 'Kpuu_duod', 'Kpuu_stom', 'Solubility',
                  'Kpuu_colon', 'BDM', 'Km_met_vitro', 'Vmax_met_vitro', 'Kpuu_liver']]).values
C_max = df['C_max'].values
t_max = df['t_max'].values
AUC = df['AUC/dose'].values
#---------------------
r_s = np.linspace(0.01,0.99 ,n_per)
print(r_s)
r_2_cmax_train = np.zeros(len(r_s))
r_2_tmax_train = np.zeros(n_per)
r_2_AUC_train = np.zeros(n_per)
r_2_cmax_test = np.zeros(n_per)
r_2_tmax_test = np.zeros(n_per)
r_2_AUC_test = np.zeros(n_per)
r_2_cmax_dif = np.zeros(n_per)
r_2_tmax_dif = np.zeros(n_per)
r_2_AUC_dif = np.zeros(n_per)
#--------------------
count = 0
for  i in r_s :
    X_train, X_test, y_train, y_test = train_test_split(X, C_max,test_size = i,random_state = 42 )
    X_train, X_test, z_train, z_test = train_test_split(X, t_max,test_size = i,random_state = 42)
    X_train, X_test, m_train, m_test = train_test_split(X, AUC,test_size = i,random_state = 42)
   
    #---------------------------------
    #hyperparameters optimazation
    max_features =['auto','sqrt','log2']
    min_samples_split = [2,4,8]
    min_samples_leaf = [2,4,8]
    random_grid ={'max_features': max_features,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'n_estimators' :[10]
                 }
    #--------------------
    clf_cmax = RandomForestRegressor()
    clf_random_cmax = RandomizedSearchCV(estimator = clf_cmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
    clf_random_cmax.fit(X_train,y_train)
    optimized_clf_cmax = clf_random_cmax.best_params_
    clf_cmax.set_params(max_features=optimized_clf_cmax['max_features'],min_samples_split=optimized_clf_cmax['min_samples_split'],
               min_samples_leaf=optimized_clf_cmax['min_samples_leaf'],n_estimators=optimized_clf_cmax['n_estimators']
                  )
    clf_cmax.fit(X_train,y_train)
    c_max_train = clf_cmax.predict(X_train)
    c_max_test = clf_cmax.predict(X_test)
    r_2_cmax_train[count] = clf_cmax.score(y_train, c_max_train)
    r_2_cmax_test[count] =clf_cmax.score(y_test, c_max_test)
    r_2_cmax_dif[count] = abs(r_2_cmax_train[count]-r_2_cmax_test[count])
    #------------------------
    clf_tmax = RandomForestRegressor()
    clf_random_tmax = RandomizedSearchCV(estimator = clf_tmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
    clf_random_tmax.fit(X_train,z_train)
    optimized_clf_tmax = clf_random_tmax.best_params_
    clf_tmax.set_params(max_features=optimized_clf_tmax['max_features'],min_samples_split=optimized_clf_tmax['min_samples_split'],
               min_samples_leaf=optimized_clf_tmax['min_samples_leaf'],n_estimators=optimized_clf_tmax['n_estimators']
                  )
    clf_tmax.fit(X_train,z_train)
    t_max_train = clf_tmax.predict(X_train)
    t_max_test = clf_tmax.predict(X_test)
    r_2_tmax_train[count] = clf_tmax.score(z_train, t_max_train)
    r_2_tmax_test[count] = clf_tmax.score(z_test, t_max_test)
    r_2_tmax_dif[count] = abs(r_2_tmax_train[count]-r_2_tmax_test[count])
    #----------------------------------
    clf_AUC = RandomForestRegressor()
    clf_random_AUC = RandomizedSearchCV(estimator = clf_AUC, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
    clf_random_AUC.fit(X_train,m_train)
    optimized_clf_AUC = clf_random_AUC.best_params_
    clf_AUC.set_params(max_features=optimized_clf_AUC['max_features'],min_samples_split=optimized_clf_AUC['min_samples_split'],
               min_samples_leaf=optimized_clf_AUC['min_samples_leaf'],n_estimators=optimized_clf_AUC['n_estimators']
                  )
    clf_AUC.fit(X_train,m_train)
    AUC_train = clf_AUC.predict(X_train)
    AUC_test = clf_AUC.predict(X_test)
    r_2_AUC_train[count] = clf_AUC.score(m_train, AUC_train)
    r_2_AUC_test[count] =clf_AUC.score(m_test, AUC_test)
    r_2_AUC_dif[count] = abs(r_2_AUC_train[count]-r_2_AUC_test[count])
    #------------------------------
    count =count+1
#------------------
width = 0.01
r_s2 =r_s+width
r_s2 =[a for a in r_s2]
r_s3 =r_s+2*width
r_s3 =[a for a in r_s3]
r_ss = str(r_s)
r_s =[a for a in r_s]

print(r_s)
r_2_cmax_train = [a for a in r_2_cmax_train]
r_2_cmax_test = [a for a in r_2_cmax_test]
r_2_cmax_dif = [a for a in r_2_cmax_dif]
r_2_tmax_train = [a for a in r_2_tmax_train]
r_2_tmax_test =[a for a in r_2_tmax_test]
r_2_tmax_dif =[a for a in r_2_tmax_dif]
r_2_AUC_train = [a for a in r_2_AUC_train]
r_2_AUC_test = [a for a in r_2_AUC_test]
r_2_AUC_dif = [a for a in r_2_AUC_dif]
'''r_2_MRT_train = [a for a in r_2_MRT_train]
r_2_MRT_test =[a for a in r_2_MRT_test]
r_2_MRT_dif =[a for a in r_2_MRT_dif]'''
legends = ['Trainig MSE','Testing MSE','difference in MSE']
plt.figure()
plt.suptitle('Optimizing testing data size',fontsize =16)
plt.subplot(131)
plt.bar(r_s,r_2_cmax_train,color='r',align='center',width= width,edgecolor= 'black',label = 'Trainig MSE',hatch = '/')
plt.bar(r_s2,r_2_cmax_test,color='gray',align='center',width = width,edgecolor= 'black',label = 'Testing MSE',hatch = '.')
plt.bar(r_s3,r_2_cmax_dif,color='skyblue',align='center',width = width,edgecolor= 'black',label = 'difference in MSE',hatch = '|')
plt.xlabel('Fraction of testing data from the original dataset')
plt.ylabel('MSE')
plt.title('C_max classifier')
plt.legend(legends)
plt.grid()
plt.subplot(132)
plt.bar(r_s,r_2_tmax_train,color='r',align='center',width = width,edgecolor= 'black',label = 'Trainig MSE',hatch ='/')
plt.bar(r_s2,r_2_tmax_test,color='gray',align='center',width =width,edgecolor= 'black',label = 'Testing MSE',hatch ='.')
plt.bar(r_s3,r_2_tmax_dif,color='skyblue',align='center',width = width,edgecolor= 'black',label = 'difference in MSE',hatch ='|')
plt.xlabel('Fraction of testing data from the original dataset')
plt.ylabel('MSE')
plt.title('t_max classifier')
plt.legend(legends)
plt.grid()
plt.subplot(133)
plt.bar(r_s,r_2_AUC_train,color='r',align='center',width = width,edgecolor= 'black',label = 'Trainig MSE',hatch ='/')
plt.bar(r_s2,r_2_AUC_test,color='gray',align='center',width = width,edgecolor= 'black',label = 'Testing MSE',hatch ='.')
plt.bar(r_s3,r_2_AUC_dif,color='skyblue',align='center',width = width,edgecolor= 'black',label = 'difference in MSE',hatch ='|')
plt.xlabel('Fraction of testing data from the original dataset')
plt.ylabel('MSE')
plt.title('AUC/dose classifier')
plt.legend(legends)
plt.grid()
plt.legend(legends)
plt.grid()
plt.show()
