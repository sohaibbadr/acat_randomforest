import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.datasets import make_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)

#--------------------------------
df = pd.read_csv('params_HR.csv',sep='\t',header = 0)

X = df[['Kpuu_colon', 'BDM', 'dose', 'Vmax_met_vitro', 'Km_met_vitro', 'Kpuu_liver']].values
df_labels = df[['C_max','t_max','AUC/dose']]
labels = df_labels.values

X_train, X_test, y_train, y_test = train_test_split(X, labels,test_size = 0.5,random_state = 42 )
#hyperparameters optimazation
max_features =['auto','sqrt','log2']
min_samples_split = [2,4,8]
min_samples_leaf = [2,4,8]
random_grid ={'max_features': max_features,
           'min_samples_split': min_samples_split,
           'min_samples_leaf': min_samples_leaf,
           'n_estimators' :[150]
             }
#---------------------
#random forest multioutput
clf_RF = RandomForestRegressor()
clf_random_RF = RandomizedSearchCV(estimator = clf_RF, param_distributions = random_grid, cv = 3, verbose=2, random_state=42 , n_jobs = -1)
clf_random_RF.fit(X_train,y_train)
optimized_clf_RF = clf_random_RF.best_params_
clf_RF.set_params(max_features=optimized_clf_RF['max_features'],min_samples_split=optimized_clf_RF['min_samples_split'],
           min_samples_leaf=optimized_clf_RF['min_samples_leaf'],n_estimators=optimized_clf_RF['n_estimators']
              )
clf_RF.fit(X_train,y_train)
importance_ = clf_RF.feature_importances_
# end of regressor training

RF_train = clf_RF.predict(X_train)
RF_test = clf_RF.predict(X_test)
RF_train_s = clf_RF.score(X_train, y_train)
RF_test_s = clf_RF.score(X_test, y_test)
#accurecy plots preperatin
#---------------------
cmax_test = y_test[:,0]
tmax_test = y_test[:,1]
cl_test = y_test[:,2]
#--------------------------
cmax_predict = RF_test[:,0]
tmax_predict = RF_test[:,1]
cl_predict = RF_test[:,2]
#------------------
cmax_error = np.zeros(len(cmax_predict))
tmax_error = np.zeros(len(tmax_predict))
cl_error = np.zeros(len(cl_predict))
for i in range(len(y_test)):
    cmax_error[i] = ((cmax_test[i]-cmax_predict[i])/cmax_test[i])*100
    tmax_error[i] = ((tmax_predict[i]-tmax_test[i])/tmax_test[i])*100
    cl_error[i] = ((cl_test[i]-cl_predict[i])/cl_test[i])*100
#----------------
cmax_error = [a for a in cmax_error]
tmax_error = [a for a in tmax_error]
cl_error = [a for a in cl_error]
#---------------
BINS_cmax = np.linspace(-100,100,11)
BINS_cmax = [x for x in BINS_cmax]
BINS_tmax = np.linspace(-100,100,11)
BINS_tmax = [x for x in BINS_tmax]
BINS_AUC = np.linspace(-100,100,11)
BINS_AUC = [x for x in BINS_AUC]
#-------------
y_hb = cmax_test*1.1
y_lb = cmax_test*0.9
y_hb = [a for a in y_hb]
y_lb = [a for a in y_lb]
#--------------
z_hb = tmax_test*1.1
z_lb = tmax_test*0.9
z_hb = [a for a in z_hb]
z_lb = [a for a in z_lb]
#----------------
m_hb = cl_test*1.1
m_lb = cl_test*0.9
m_hb = [a for a in m_hb]
m_lb = [a for a in m_lb]
#---------------
cmax_test =[a for a in cmax_test]
cmax_predict = [a for a in cmax_predict]
tmax_test =[a for a in tmax_test]
tmax_predict = [a for a in tmax_predict]
cl_test =[a for a in cl_test]
cl_predict = [a for a in cl_predict]
#---------------
plt.figure(1)
plt.subplot(131)
plt.hist(cmax_error,bins =BINS_cmax, color = 'green',ec ='black',normed =True)
plt.xlabel('%Error')
plt.ylabel('Probability/binwidth')
plt.xticks(np.asanyarray(BINS_cmax))
plt.xticks(rotation = 90)
plt.title('C_max MO_Reduced')
plt.grid()
#--------------------
plt.subplot(132)
plt.hist(tmax_error,bins =BINS_tmax, color = 'green',ec = 'black',normed =True)
plt.xlabel('%Error')
plt.title('t_max MO_Reduced')
plt.xticks(np.asanyarray(BINS_tmax))
plt.xticks(rotation = 90)

plt.grid()
#-------------------
plt.subplot(133)
plt.hist(cl_error,bins =BINS_AUC, color = 'green',ec = 'black',normed =True)
plt.xlabel('%Error')
plt.title('(Clearance^(-1)) MO_Reduced' )
plt.xticks(np.asanyarray(BINS_AUC))
plt.xticks(rotation = 90)
plt.grid()
#------------------
print(RF_train_s,RF_test_s)
plt.legend()
plt.show()
