import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.datasets import make_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)
#--------------------------------

n_per = 15
n_features = 19
df = pd.read_csv('params_HR.csv',sep='\t',header = 0)
df.rename( columns={'Unnamed: 0':'a'}, inplace=True )

df_features = df.drop(['a','C_max', 't_max', 'AUC', 'AUC_inf', 'AUC/dose', 'MRT'],axis =1)
features = np.array(df_features.columns)
X = df_features.values
C_max = df['C_max'].values
t_max = df['t_max'].values
AUC = df['AUC/dose'].values
MRT = df['MRT'].values

r_s = np.random.random_integers(10,500 ,size=n_per)
r_2_cmax = np.zeros(n_per)
r_2_tmax = np.zeros(n_per)
r_2_AUC = np.zeros(n_per)
r_2_MRT = np.zeros(n_per)
importance_cmax = np.zeros((n_per,n_features))
importance_tmax = np.zeros((n_per,n_features))
importance_AUC = np.zeros((n_per,n_features))
importance_MRT = np.zeros((n_per,n_features))
count = 0
for  i in r_s :
    X_train, X_test, y_train, y_test = train_test_split(X, C_max,test_size = 0.5,random_state = i )
    X_train, X_test, z_train, z_test = train_test_split(X, t_max,test_size = 0.5,random_state = i)
    X_train, X_test, m_train, m_test = train_test_split(X, AUC,test_size = 0.5,random_state = i)
    X_train, X_test, k_train, k_test = train_test_split(X, MRT,test_size = 0.5,random_state = i)
    #---------------------------------
    #hyperparameters optimazation
    max_features =['auto','sqrt','log2']
    min_samples_split = [2,4,8]
    min_samples_leaf = [2,4,8]
    random_grid ={'max_features': max_features,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'n_estimators' :[10]
                 }
    #--------------------
    #optimizing random forest hyper parameters for c_max
    clf_cmax = RandomForestRegressor()
    clf_random_cmax = RandomizedSearchCV(estimator = clf_cmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=i , n_jobs = -1)
    clf_random_cmax.fit(X_train,y_train)
    optimized_clf_cmax = clf_random_cmax.best_params_
    clf_cmax.set_params(max_features=optimized_clf_cmax['max_features'],min_samples_split=optimized_clf_cmax['min_samples_split'],
               min_samples_leaf=optimized_clf_cmax['min_samples_leaf'],n_estimators=optimized_clf_cmax['n_estimators']
                  )
    clf_cmax.fit(X_train,y_train)
    importance_cmax[count] = clf_cmax.feature_importances_
    r_2_cmax[count] = clf_cmax.score(X_test,y_test)
    #-----------------------
    #optimizing random forest hyper parameters for t_max
    clf_tmax = RandomForestRegressor()
    clf_random_tmax = RandomizedSearchCV(estimator = clf_tmax, param_distributions = random_grid, cv = 3, verbose=2, random_state=i , n_jobs = -1)
    clf_random_tmax.fit(X_train,z_train)
    optimized_clf_tmax = clf_random_tmax.best_params_
    clf_tmax.set_params(max_features=optimized_clf_tmax['max_features'],min_samples_split=optimized_clf_tmax['min_samples_split'],
               min_samples_leaf=optimized_clf_tmax['min_samples_leaf'],n_estimators=optimized_clf_tmax['n_estimators']
                  )
    clf_tmax.fit(X_train,z_train)
    importance_tmax[count] = clf_tmax.feature_importances_
    r_2_tmax[count] = clf_tmax.score(X_test,z_test)
    #----------------------------------
    #optimizing random forest hyper parameters for AUC
    clf_AUC = RandomForestRegressor()
    clf_random_AUC = RandomizedSearchCV(estimator = clf_AUC, param_distributions = random_grid, cv = 3, verbose=2, random_state=i , n_jobs = -1)
    clf_random_AUC.fit(X_train,m_train)
    optimized_clf_AUC = clf_random_AUC.best_params_
    clf_AUC.set_params(max_features=optimized_clf_AUC['max_features'],min_samples_split=optimized_clf_AUC['min_samples_split'],
               min_samples_leaf=optimized_clf_AUC['min_samples_leaf'],n_estimators=optimized_clf_AUC['n_estimators']
                  )
    clf_AUC.fit(X_train,m_train)
    importance_AUC[count] = clf_AUC.feature_importances_
    r_2_AUC[count] = clf_AUC.score(X_test,m_test)
    '''#optimizing random forest hyper parameters for MRT
    clf_MRT = RandomForestRegressor()
    clf_random_MRT = RandomizedSearchCV(estimator = clf_MRT, param_distributions = random_grid, cv = 3, verbose=2, random_state=i , n_jobs = -1)
    clf_random_MRT.fit(X_train,k_train)
    optimized_clf_MRT = clf_random_MRT.best_params_
    clf_MRT.set_params(max_features=optimized_clf_MRT['max_features'],min_samples_split=optimized_clf_MRT['min_samples_split'],
               min_samples_leaf=optimized_clf_MRT['min_samples_leaf'],n_estimators=optimized_clf_MRT['n_estimators']
                  )
    clf_MRT.fit(X_train,k_train)
    importance_MRT[count] = clf_MRT.feature_importances_
    r_2_MRT[count] = clf_MRT.score(X_test,k_test)'''
    count = count+1
#---------------------
r_2_cmax_mean =np.average(r_2_cmax)
r_2_tmax_mean =np.average(r_2_tmax)
r_2_AUC_mean =np.average(r_2_AUC)
#r_2_MRT_mean =np.average(r_2_MRT)
r_2_cmax_std =np.std(r_2_cmax)
r_2_tmax_std =np.std(r_2_tmax)
r_2_AUC_std =np.std(r_2_AUC)
#r_2_MRT_std =np.std(r_2_MRT)
#----------------------------
features_cmax_mean =np.zeros(n_features)
features_tmax_mean =np.zeros(n_features)
features_AUC_mean =np.zeros(n_features)
#features_MRT_mean =np.zeros(n_features)
features_cmax_std =np.zeros(n_features)
features_tmax_std =np.zeros(n_features)
features_AUC_std =np.zeros(n_features)
#features_MRT_std =np.zeros(n_features)
for j in range(n_features):
    features_cmax_mean[j] =np.mean(importance_cmax[:,j])
    features_tmax_mean[j] =np.mean(importance_tmax[:,j])
    features_AUC_mean[j] =np.mean(importance_AUC[:,j])
    #features_MRT_mean[j] =np.mean(importance_MRT[:,j])
    features_cmax_std[j] =np.std(importance_cmax[:,j])
    features_tmax_std[j] =np.std(importance_tmax[:,j])
    features_AUC_std[j] =np.std(importance_AUC[:,j])
    #features_MRT_std[j] =np.std(importance_MRT[:,j])
#-----------------------------
features_cmax_mean_sorted,features_sorted_cmax = ACATlib.ACAT_Sorter(features_cmax_mean ,features)
features_tmax_mean_sorted,features_sorted_tmax = ACATlib.ACAT_Sorter(features_tmax_mean,features)
features_AUC_mean_sorted,features_sorted_AUC = ACATlib.ACAT_Sorter(features_AUC_mean,features)
#features_MRT_mean_sorted,features_sorted_MRT = ACATlib.ACAT_Sorter(features_MRT_mean,features)
features_cmax_mean_sorted,features_cmax_std_sorted = ACATlib.ACAT_Sorter(features_cmax_mean ,features_cmax_std)
features_tmax_mean_sorted,features_tmax_std_sorted = ACATlib.ACAT_Sorter(features_tmax_mean,features_tmax_std)
features_AUC_mean_sorted,features_AUC_std_sorted = ACATlib.ACAT_Sorter(features_AUC_mean,features_AUC_std)
#eatures_MRT_mean_sorted,features_MRT_std_sorted = ACATlib.ACAT_Sorter(features_MRT_mean,features_MRT_std)
#--------------------------
print(features_sorted_cmax)
print(features_sorted_tmax)
print(features_sorted_AUC)
'''plt.figure(1)
x_axis_fig1 = ['c_max_clf','t_max_clf','AUC_clf']
y_axis_fig1 = [r_2_cmax_mean,r_2_tmax_mean,r_2_AUC_mean]
error = [r_2_cmax_std,r_2_tmax_std,r_2_AUC_std]
plt.bar(x_axis_fig1, y_axis_fig1, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10,color = 'red',edgecolor = 'black',width= 0.25)
plt.xticks(x_axis_fig1)
#plt.ylim((0.7,1))
plt.xticks(rotation =90)
plt.title('Classifier')
plt.grid(True)'''
#------------------
plt.figure(2)
plt.subplot(131)
x = np.arange(len(features_sorted_cmax))
plt.bar(x,features_cmax_mean_sorted,yerr = features_cmax_std_sorted,alpha=0.5,color = 'skyblue',capsize=10,edgecolor ='black',align='center',ecolor='black')
plt.xlabel('features')
plt.xticks(x ,features_sorted_cmax)               
plt.xticks(rotation=90)
plt.ylabel('importance')
plt.yscale('log')
plt.title('C_max classifier')
plt.subplot(132)
plt.bar(x,features_tmax_mean_sorted,yerr = features_tmax_std_sorted ,color = 'skyblue',capsize=10,alpha=0.5,edgecolor ='black',align='center',ecolor='black')
plt.xlabel('features')
plt.xticks(x ,features_sorted_tmax)               
plt.xticks(rotation=90)
plt.ylabel('importance')
plt.yscale('log')
plt.title('t_max classifier')
plt.subplot(133)
plt.bar(x,features_AUC_mean_sorted,yerr = features_AUC_std_sorted ,color = 'skyblue',capsize=10,alpha=0.5,edgecolor ='black',align='center',ecolor='black')
plt.xlabel('features')
plt.xticks(x ,features_sorted_AUC)
plt.xticks(rotation=90)
plt.ylabel('importance')
plt.yscale('log')
plt.title('AUC/dose classifier')
'''plt.subplot(224)
plt.bar(features_sorted_MRT,features_MRT_mean_sorted,yerr = features_MRT_std_sorted ,color = 'skyblue',capsize=10,alpha=0.5,edgecolor ='black',align='center',ecolor='black')
plt.xlabel('features')
plt.xticks(rotation=90)
plt.ylabel('importance')'''
plt.yscale('log')

plt.tight_layout()
plt.show()
